<?php ini_set("display_errors",1);

if(php_sapi_name() != "cli"){
	die('This script needs command line interface (CLI) to run.');
}else{
	
	$script = $argv[1];
	
	/**
	Every scraper FILE should be added
	in the $source array variable (below)
	as it ispart of the LOGIC and should
	be named exactly as the FILE name.
	**/
	$source = array(
		'www.gilroyhyundai.com' => 'gilroyhyundai.php',
		'www.gilroynissan.com' 	=> 'gilroynissan.php'
	);
	
	if(isset($script) && file_exists('scraper/'.$source[$script])){
	
		$dbhost = "localhost";
		$dbuser = "root";
		$dbpass = "MYSQLADMIN";
		$dbname = "fairground";
		$dbtable = "wp_cse_list";
		
		mysql_connect($dbhost,$dbuser,$dbpass) or die("Unable to connect MySQL server : ".mysql_error());
		mysql_select_db($dbname) or die("Unable to connect database : ".mysql_error());
		
		$create_table = "CREATE TABLE IF NOT EXISTS $dbtable (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) DEFAULT NULL,
		  `make` varchar(255) DEFAULT NULL,
		  `model` varchar(255) DEFAULT NULL,
		  `year` year(4) DEFAULT NULL,
		  `vin` varchar(255) DEFAULT NULL,
		  `mileage` int(11) DEFAULT NULL,
		  `engine` varchar(255) DEFAULT NULL,
		  `transmission` varchar(255) DEFAULT NULL,
		  `price` double(20,2) DEFAULT NULL,
		  `color` varchar(20) DEFAULT NULL,
		  `body_style` varchar(255) DEFAULT NULL,
		  `images` longtext,
		  `features` longtext,
		  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `source` varchar(20) DEFAULT NULL,
		  PRIMARY KEY (`id`),
		  KEY `make` (`make`),
		  KEY `model` (`model`),
		  KEY `year` (`year`),
		  KEY `mileage` (`mileage`),
		  KEY `engine` (`engine`),
		  KEY `transmission` (`transmission`),
		  KEY `body_style` (`body_style`)
		) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
		
		$truncate_table = "DELETE FROM {$dbtable} WHERE source = '{$source[$script]}'";
		
		mysql_query($create_table) or die('Create Query Error : ' . mysql_error()); //table should be created if NOT EXISTS
		mysql_query($truncate_table) or die('Truncate Query Error : ' . mysql_error()); //truncate OLD data
		
		include_once('scraper/'.$source[$script]);
	}

}

function crawl($weburl){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $weburl);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_ENCODING, '');
	$output = curl_exec($ch);
	return $output;
}

function dom($domHTML,$query){
	$dom = new DOMDocument();
	@$dom->loadHTML($domHTML);
			
	$xpath = new DomXPath($dom);
	return $xpath->query($query);
}

function addToURL($html){
	global $urls;
	
	if(!empty($html)){
		foreach($html as $htm){
		
			$url = $htm->getAttributeNode('href')->nodeValue;
			
			if(!in_array($url, $urls)){
				$urls[] = $url;
			}
			
		}
		return $urls;
	}
}
