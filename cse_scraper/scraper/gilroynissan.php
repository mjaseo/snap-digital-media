<?php

$urls = $vehicles = array();
$main = "http://www.gilroynissan.com";

print "\nSCRAPING STARTED @ " . date('Y-m-d H:i:s') . " ($main) \n";

/**
We crawl the MAIN INVENTORY PAGE
and set PER PAGE TO MAX (100)
**/
$webpage = crawl($main . "/inventory/newsearch/records100/");

/**
From the $main above, we parse each vehicle 
by its URL and save it into `addToURL()` function
**/
$html = dom($webpage,'//h2[@class="vehicletitle"]/a');
addToURL($html);

/**
We need to check for the REMAINING pages
and CRAWL it as well if found
**/
$next = dom($webpage,'//li[@class="next"]/a');
$tmp = array();

foreach($next as $nxt){

	$url = $nxt->getAttributeNode('href')->nodeValue;
	
	if(!in_array($url,$tmp)){ //we make sure that WE DON'T ACCESS duplicate URL as setup in the WEBSITE
		
		$nextpage = crawl($main . $url);
		$nexthtml = dom($nextpage,'//h2[@class="vehicletitle"]/a');
		addToURL($nexthtml);
		
		$tmp[] = $url;
	}
}

//print '<pre>';
/**
Now we CRAWL each URL in the $urls array
for each vehicle details
**/
if(!empty($urls)):

	foreach($urls as $k=>$url){
	
		$page = crawl($main . $url);
		
		if(preg_match('/var BehindAuto \=(.*)\;var AutoTitle \= "(.*)"\;/', $page, $auto)){
			$json = json_decode($auto[1]);
			
			$vehicles[$k]['name'] = $auto[2];
			$vehicles[$k]['make'] = $json->Make;
			$vehicles[$k]['model'] = $json->Model;
			$vehicles[$k]['vin'] = $json->VIN;
			$vehicles[$k]['mileage'] = $json->Mileage;
			$vehicles[$k]['engine'] = $json->EngineName;
			$vehicles[$k]['transmission'] = $json->TransmissionName;
			$vehicles[$k]['price'] = $json->Price;
			$vehicles[$k]['color'] = $json->ExternalColorName;
			$vehicles[$k]['body_style'] = $json->DriveLine . ' ' . $json->BodyStyle . ' (' . $json->Doors . ' Door)';
			$vehicles[$k]['year'] = $json->YearName;
			
		}
		
		$zoom = dom($page,'//a[@class="showFancybox"]');
		
		if($zoom->length > 0){
			$photos = dom(crawl($main . $zoom->item(0)->getAttributeNode('href')->nodeValue),'//img[@class="image fullsize"]');
			
			if(!empty($photos)){
				$images = array();
				foreach($photos as $photo) $images[] = $photo->getAttributeNode('rel')->nodeValue;
				$vehicles[$k]['images'] = serialize($images);
			}
		}else{
			$noimage = dom($page, '//div[@class="vehicleDetailsFullSizePhoto"]/img');
			$vehicles[$k]['images'] = serialize(array('http://edge.vinsolutions.com/images/live/NoPhotoImages/0000/0/0.jpg'));
		}
			
		$option = dom($page,'//li/h4');
		$detail = dom($page,'//ul[@class="bullets"]');
		
		if(!empty($option)){
			$features = array();
			foreach($option as $o=>$opt){
				$dtl = $detail->item($o)->c14n();
				if($dtl) $features[$opt->nodeValue] = strip_tags(preg_replace("/<\/li>/","\n",$dtl));
			}
			$vehicles[$k]['features'] = serialize($features);
		}
		
		$vehicles[$k]['source'] = array_search($source[$script],$source);
		
		if(is_array($vehicles[$k]) && !empty($vehicles[$k])){
	
				$keys = array_keys($vehicles[$k]);
				$vals = array_values($vehicles[$k]);
				
				$fields = implode(",",$keys);
				$values = "(";
				
				foreach($vals as $v){
					$values .= '"'.mysql_real_escape_string($v).'",';
				}
				
				$sql = "INSERT INTO {$dbtable}(".$fields.") VALUES ". rtrim($values,","). ")";
				mysql_query($sql) or die("Query Error : ".mysql_error());
				
				if(isset($vehicles[$k]['images'])) unset($vehicles[$k]['images']);
				if(isset($vehicles[$k]['features'])) unset($vehicles[$k]['features']);
				
				print_r($vehicles[$k]);
			
		}
		
		sleep(rand(3,5));
	
	}
	
endif;

print "\nSCRAPING ENDED @ " . date('Y-m-d H:i:s') . " ($main) \n\n";