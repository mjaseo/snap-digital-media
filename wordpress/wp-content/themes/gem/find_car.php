<?php
/*
Template Name: Find Car
*/
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
add_action( 'genesis_meta', 'outreach_findcar_genesis_meta' );

function outreach_findcar_genesis_meta() {

	if ( is_active_sidebar( 'findcar-featured' ) || is_active_sidebar( 'findcar-1' ) || is_active_sidebar( 'findcar-2' ) || is_active_sidebar( 'findcar-3' ) || is_active_sidebar( 'findcar-4' ) || is_active_sidebar( 'findcar-contents' ) ) {

		remove_action( 'genesis_loop', 'genesis_do_loop' );
		add_action( 'genesis_before_loop', 'outreach_findcar_featured' );
		add_action( 'genesis_before_loop', 'outreach_searchcar' );
		add_action( 'genesis_loop', 'outreach_findcar_sections', 3 );
		add_action( 'genesis_after_loop', 'outreach_findcar_contents' );
 		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
		add_filter( 'body_class', 'add_body_class' );

		function add_body_class( $classes ) {
   			$classes[] = 'outreach';
  			return $classes;
		}

	}
}

function outreach_findcar_featured() {

	if ( is_active_sidebar( 'findcar-featured' ) ) {
	   genesis_widget_area( 'findcar-featured', array(
	       'before' => '<div class="home-featured widget-area">'
	   ) );
	}

}

function outreach_searchcar() { ?>
<div class="searchcar">
<form method="post" action="<?php echo site_url() . '/results/'; ?>">
<select name="selectmake" id="selectmake">
<option value="">Any Make</option>
<?php
	if(is_plugin_active('custom-search-engine/index.php') && $make = wpcse_filter('make')){
		foreach($make as $mk) echo '<option value="'.urlencode($mk->make).'">'.$mk->make.'</option>';	
	}
?>
</select>
<select name="selectmodel" id="selectmodel">
<option value="">Any Model</option>
</select>
<select name="selectyear" id="selectyear">
<option value="">Any Year</option>
</select>
<select name="selectprice" id="selectprice">
<option value="">Any Price</option>
<?php
	if(is_plugin_active('custom-search-engine/index.php')){
		wpcse_price();	
	}
?>
</select>
<input type="submit" value="Find My Car!">
</form>
</div><!--searchcar-->
<?php }

function outreach_findcar_sections() {

	if ( is_active_sidebar( 'findcar-1' ) || is_active_sidebar( 'findcar-2' ) || is_active_sidebar( 'findcar-3' ) || is_active_sidebar( 'findcar-4' ) ) {

		echo '<div id="home-sections"><div class="wrap">';

		   genesis_widget_area( 'findcar-1', array(
		       'before' => '<div class="home-1 widget-area">',
		   ) );

		   genesis_widget_area( 'findcar-2', array(
		       'before' => '<div class="home-2 widget-area">',
		   ) );

		   genesis_widget_area( 'findcar-3', array(
		       'before' => '<div class="home-3 widget-area">',
		   ) );

		   genesis_widget_area( 'findcar-4', array(
		       'before' => '<div class="home-4 widget-area">',
		   ) );

		echo '</div><!-- end .wrap --></div><!-- end #findcar-sections -->';

	}

}
 
 
/** car find loop */
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'car_find_loop' );
 
function car_find_loop() { ?>

	<div class="product-list-wrapper">
	
		<div class="findcar-contents">
			<div class="wrap">
				<h1>
					Featured Vehicles
				</h1>
			</div>
		</div>
	
	<?php
		
		if( is_plugin_active('custom-search-engine/index.php') ){
			
			wpcse_reset(); //clear SESSION first
			
			$vehicles = wpcse_findcar();
			
			if($vehicles){ 
			
				echo '<ul class="tricol-product-list">';
				
					foreach($vehicles as $vehicle){
					
						$container = ($vehicle->mileage > 0) ? 'Used' : 'New';
						$image = unserialize($vehicle->images);
		
						echo '<li class="new-arrivals-list">';
						echo 	'<a class="arrivals-link" href="'. site_url() .'/detail/?vin='. $vehicle->vin .'">';
						echo 		'<div class="cpsAjaxLoaderHome"></div>';
						echo 		'<div class="image-container">';
						echo 			'<div class="'. $container .'"></div>';
						echo 			'<img alt="mdx-1" class="attachment-medium" src="'. (($image[0]) ? $image[0] : 'http://edge.vinsolutions.com/images/live/NoPhotoImages/0000/0/0.jpg') .'">';
						echo 		'</div>';
						echo 	'</a>';
						echo 	'<div class="arrivals-details">';
						echo 		'<p><strong>'. wp_trim_words($vehicle->name, 4) .'</strong></p>';
						echo 		'<div class="meta-style">'. $vehicle->year .' | '. number_format($vehicle->mileage) .' Miles</div>';
						echo 		'<div class="price-style">$'. number_format($vehicle->price,2,".",",") .'</div>';
						echo 		'<p class="post_views"> <strong>7612 Views</strong></p>';
						echo 		'<div style="clear: both"></div>';
						echo 		'<p class="btnp"> <a class="detail-btn" href="'. site_url() .'/detail/?vin='. $vehicle->vin .'"> View Details</a></p>';
						echo 	'</div>';
						echo '</li>';
		
					}//end foreach
					
				echo '</ul>';
	
			}else{
				echo '<h4 style="text-align:center;color:red">No Vehicles Found.</h4>';
			}
	
		} else {
			echo '<h4 style="text-align:center;color:red">No Vehicles Found. Please make sure Custom Search Engine plugin is active.</h4>';
		}
	
	?>
	</div>
	
	<script type="text/javascript">
	
	$(document).ready(function(){
	
		$('#selectmake').change(function(){
			
			var make = $(this).val();
			var model = $('#selectmodel');
			var year = $('#selectyear');
			
			if(make.length > 0){
			
				model.prop("disabled",true);
				year.prop("disabled",true);
				
				$.ajax({
					url:"<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
					type:'POST',
					data: {
						action : 'wpcse_filter_callback',
						make : make,
						model : model.val()
					},
					success: function(res){
						
						try{
							var resp = $.parseJSON(res);
							
							if(resp.model != ""){
								model.empty();
								reset('#selectmodel','Any Model');
								model.append(resp.model);
								model.prop("disabled",false);
							}
							
							if(resp.year != ""){
								year.empty();
								reset('#selectyear','Any Year');
								year.append(resp.year);
								year.prop("disabled",false);
							}
							
						}catch(e){
							alert(e);
						}
						
					}
				});
				
			}else{
				reset('#selectmodel','Any Model');
				reset('#selectyear','Any Year');
			}
			
		});
		
		$('#selectmodel').change(function(){
			var make = $('#selectmake').val();
			var model = $(this).val();
			var year = $('#selectyear');
			
			if(model.length > 0){
			
				year.prop("disabled",true);
				
				$.ajax({
					url:"<?php echo site_url(); ?>/wp-admin/admin-ajax.php",
					type:'POST',
					data: {
						action : 'wpcse_filter_callback',
						make : make,
						model : model
					},
					success: function(res){
						
						try{
							var resp = $.parseJSON(res);
							
							if(resp.year != ""){
								year.empty();
								reset('#selectyear','Any Year');
								year.append(resp.year);
								year.prop("disabled",false);
							}
							
						}catch(e){
							alert(e);
						}
						
					}
				});
				
			}else{
				$('#selectmake').change();
			}
		})
		
		var reset = function(id,option){
			$(id).empty();
			$(id).append('<option value="">' + option + '</option>');
		}
		
	});
	
	</script>

<?php } //end FUNCTION


function outreach_findcar_contents() {

	if ( is_active_sidebar( 'findcar-contents' ) ) {
	   genesis_widget_area( 'findcar-contents', array(
	       'before' => '<div class="findcar-contentsx home-contents widget-area">'
	   ) );
	}

} 
genesis();