<?php
/*
Template Name: Results
*/
//* Force full-width-content layout setting
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
/** car find loop */
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'car_find_loop' );
 
function car_find_loop() { 

	if(is_plugin_active('custom-search-engine/index.php')){
	
		$query = wpcse_query();
		$results = wpcse_findcar('',$query->where,$query->sort);
		$count = wpcse_findcar_count($query->where);
?>
<div class="sevo atcui-page-title ">
		<a name="top"></a>
        <div class="atcui-column atcui-span-17">
		<span class="sevo atcui-page-title-prominent">Search Results </span>
        </div>

        <div class="atcui-column atcui-column-right num-listings-container">

        <div class="atcui-column">
		<span class="num-listings"><?php echo number_format($count); ?></span>
        </div>

        <div class="atcui-column num-listings-text">listings<span class="atcui-block">matching your criteria</span>
        </div>
        </div>
    </div>
 
<!-----start sidebar----->
<div class="search-summary">
	<div class="atcui-title">
		Your Search
	</div>
	<div class="atcui-section atcui-clearfix">
		<div class= "atcui-section atcui-clearfix atcui-small findcar-filterSummary">
			<span class="atcui-section-title">Make</span>
			<ul class="atcui-list atcui-bare selected-filters">
				<li class="atcui-list-item">
					<a class="atcui-icon-link" href="<?php echo site_url(); ?>/results/?unset=make"><?php echo (!empty($query->make)) ? $query->make : 'Any Make'; ?> <span class= "atcui-icon atcui-remove">&nbsp;</span></a>
				</li>
			</ul>
		</div>
		
		<div class= "atcui-section atcui-clearfix atcui-small findcar-filterSummary">
			<span class="atcui-section-title">Model</span>
			<ul class="atcui-list atcui-bare selected-filters">
				<li class="atcui-list-item">
					<a class="atcui-icon-link" href="<?php echo site_url(); ?>/results/?unset=model"><?php echo (!empty($query->model)) ? $query->model : 'Any Model'; ?> <span class= "atcui-icon atcui-remove">&nbsp;</span></a>
				</li>
			</ul>
		</div>
	</div>
	<div class="atcui-section atcui-clearfix search-actions">
		<div class="search-summary-save-search active">
			<span class= "atcui-trigger atcui-icon-link atcui-icon-span"><span>Save this
			search</span><span class=" atcui-saveSearch icon-spinner2">&nbsp;</span></span>
		</div>
	</div>
	<div class="atcui-section atcui-clearfix search-actions last-child">
		<a class="atcui-icon-link" href="<?php echo site_url() . '/find-car/'; ?>"><span class="icon-search atcui-newSearch">&nbsp;</span>Start a new search</a>
	</div>
</div>

<!-----end sidebar------->



<!--- start search sort --->
<div class="listingsControlBar"> 
<label>Sort by</label>
<select class="sortBy-selectOneMenu" name="sortBy-selectOneMenu" size="1">
<?php
	if(is_plugin_active('custom-search-engine/index.php')){
		wpcse_sort();	
	}
?>
</select>
       
<label for="numRecords">Per page</label>
<div id="numRecords" class="selectOneMenu">
<select id="selectOneMenu" name="limit" size="1">
<?php
	if(is_plugin_active('custom-search-engine/index.php')){
		wpcse_perpage();	
	}
?>
</select>
</div>
<?php $paged = ( get_query_var('page') ) ? get_query_var('page') : 1; ?>
<span class="pageof">Page <?php echo $paged; ?> of <?php echo ($count > $query->limit) ? ceil($count / $query->limit) : 1; ?></span> 
</div>
<!--- end search sort --->



<div class="detail-page-content hideOnSearch">
	
		
		<?php if($results){ 
				
				foreach($results as $result){
				
					$image = unserialize($result->images);
					$container = ($vehicle->mileage > 0) ? 'Used' : 'New';
					$detail = site_url() . '/detail/?vin=' . $result->vin;
				
				echo 	'<div class="result-car">
							<div class="cpsAjaxLoaderResults"></div><a title="'.$result->name.'" rel="bookmark" href="'.$detail.'" class="result-car-link"><img width="227" height="140" src="'.$image[0].'" class="attachment-medium" alt="chevy-crossover-12"></a>
							<div class="vehicle-main-image">
								<span class="'.$container.'"></span>
							</div>
							<div class="result-detail-wrapper">
								<!-- result detail wrapper -->
								<p class="vehicle-name">
									<span class="mini-hide"><a href="'.$detail.'" style="color:#333">'.$result->name.'</a></span>
								</p>
								<div class="meta-style results">
									'.$result->year.' | '.$result->mileage.' Miles
								</div>
								<p class="strong">
									'.$result->make.' '.$result->model.'
								</p>
								<p class="vehicle-secondary-info hide-for-small">
														'. $result->body_style . (!empty($result->transmission) ? ' | ' . $result->transmission : '') .' | '. (!empty($result->color) ? $result->color : 'Not Specified' ) .'
													</p>
								<div class="price-style results">
									$'.number_format($result->price,2,'.',',').'
								</div>
								<div class="hide-for-small">
									<strong>2426 Views</strong>
								</div>
					 				<a href="'.$detail.'" class="get-internet-ptice">Get Internet Price!</a>
					 			<div style="clear:both;"></div>
							</div>
							<div style="clear:both;"></div>
						</div>';
				} 
				
				echo '<div style="clear:both"></div>';
				echo '<div class="bottom-pagination">
						<p>
						<a id="link" href="#top">BACK TO TOP</a>
						</p>
						<p class="paging">' . str_replace('page-numbers','convertUrl page-numbers',wpcse_paginate()) . '
						</p>
					</div>';
			}else{
				echo '<br /><h4 style="text-align:center;color:red">No Vehicles Found. Please try a <a href="'.site_url().'/find-car/">new search</a>.</h4>';
			}
		?>
		
	</div>
</div>

<script type="text/javascript">
$('.sortBy-selectOneMenu').change(function(){
	window.location = '<?php echo site_url(); ?>/results/?sort=' + $(this).val();
});
$('#selectOneMenu').change(function(){
	window.location = '<?php echo site_url(); ?>/results/?limit=' + $(this).val();
});
</script>

<?php }else{
		
		echo '<h4 style="text-align:center;color:red">No Vehicles Found. Please make sure Custom Search Engine plugin is active.</h4>';
		
	} 
}
 
genesis();