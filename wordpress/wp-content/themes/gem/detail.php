<?php
/*
Template Name: Detail
*/
//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );


include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/** car find loop */
remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'car_find_loop' );
function car_find_loop() {

	if( is_plugin_active('custom-search-engine/index.php') ){
	
		$top_deals = wpcse_findcar(5);
		
		echo '<div class="right-block">
				<div class="right-white-block mobile top-deals-mobile-wrapper">
					<h3>Top Deals</h3>
						<ul class="deal-rates">';
						
						if($top_deals){
							
							foreach($top_deals as $deal){
							
								$image = unserialize($deal->images);
							
								echo '<li class="new-arrivals-list">
								        <a class="arrivals-link" href=
								        "'. site_url() .'/detail/?vin='. $deal->vin .'">
								        <div class="image-container"><img alt="A5-7" class="attachment-medium"
								        src=
								        "'. (($image[0]) ? $image[0] : 'http://edge.vinsolutions.com/images/live/NoPhotoImages/0000/0/0.jpg') .'"></div>
								        <div class="arrivals-details">
								          <div class="meta-style title">
								            '. $deal->name .'
								          </div>
								          <div class="meta-style">
								            '. $deal->year .' | '. number_format($deal->mileage) .' Miles
								          </div>
								          <p class="location-deals">'. $deal->make .' '. $deal->model . '</p>
								          <div class="price-style">
								            $'. number_format($deal->price,2,".",",") .'
								          </div>
								        </div></a>
								        <div class="clear"></div>
								      </li>';
								
							}
							
						}
		echo 		'</ul>
				</div>
			</div>';
			
		$detail = wpcse_findcar(1,' AND vin = "'.$_REQUEST['vin'].'"');
		
		if(!$detail){
			echo '<div class="detail-page-content hideOnSearch"><h4 style="text-align:center;color:red">Car details not found. Please refine your a href="'.site_url().'/find-car/">search</a>.</h4></div>';
		}else{
		
		$images = unserialize($detail[0]->images);
		
		$feature = unserialize($detail[0]->features);
?>
<style>
.gallery{
	width:495px!important;
	height:426px!important;
}
</style>
<div class="detail-page-content hideOnSearch">
  <div class="title">
    <h1 class="hideOnSearch"><?php echo $detail[0]->name; ?></h1>
    <p class="post_views_single"><strong>7743 Views</strong></p>
  </div>
  <div style="clear: both"></div>
 
  <div class="clear"></div>
  <ul class="quick-list quick-glance hideOnSearch">
    <li>
					      <p class="strong">Make : <strong style="color:#000"><?php echo $detail[0]->make; ?></strong></p>
					    </li>
					    <li>
					      <p class="strong">Model : <strong style="color:#000"><?php echo $detail[0]->model; ?></strong></p>
					    </li>
					    <li>
					      <p class="strong">Price :</p>$<?php echo number_format($detail[0]->price,2,".",","); ?>
					    </li>
					     <li>
					      <p class="strong">Mileage :</p><?php echo number_format($detail[0]->mileage); ?> Miles
					    </li>
					    <li>
					      <p class="strong">Body Style :<br /> <strong style="color:#000"><?php echo $detail[0]->body_style; ?>
					    </strong></p></li>
					    <li>
					      <p class="strong">Transmission :<br /> <strong style="color:#000"><?php echo ((!empty($detail[0]->transmission)) ? $detail[0]->transmission : 'Not Specified'); ?>
					    </strong></p></li>
					    <li>
					      <p class="strong">Engine :</p><?php echo (!empty($detail[0]->engine) ? $detail[0]->engine : 'Not Specified'); ?>
					    </li>
					    <li>
					      <p class="strong">Color :</p><?php echo ((!empty($detail[0]->color)) ? $detail[0]->color : 'Not Specified'); ?>
					    </li>
					    <li>
					      <p class="strong">Year :</p><?php echo $detail[0]->year; ?>
					    </li>
					    <li>
					      <p class="strong">VIN :</p><?php echo $detail[0]->vin; ?>
					    </li>
					    <li style="list-style: none; display: inline">
					      <div style= "background:none; padding:10px 3px 0px 3px!important;margin:0px auto;"></div>
					    </li>
  </ul>
  <div class="big-view hideOnSearch" id="gallery_holder">
    <div class="big-view hideOnSearch" id="gallery" style="height:auto;">
    
    <?php if(count($images) > 1) //unset($images[0]); 
    	$count = count($images);
    foreach($images as $mainkey=>$image){
    ?>
      <a class="gallery cboxElement" href= "<?php echo $image; ?>"
      style= "position: absolute; top: 0px; left: 0px; display: block; z-index: <?php echo $count; ?>; opacity: 1; width:495; height:426">
      <span class="lupa"></span><img width="495" height="426" alt="mdx-<?php echo $mainkey; ?>" class="attachment-large" src="<?php echo $image; ?>"></a>
    <?php $count--; } ?>
    
      <div style= "clear: both; position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 0;"></div>
    </div>
    <div style="clear:both"></div>
    <div class="small-view hideOnSearch" style="width:100%; float:left;">
      <ul class="elastislide-list thumbnails hideOnSearch" id="nav">
      
      <?php if(count($images) > 1) //unset($images[0]); 
      foreach($images as $thumbkey=>$image){ ?>
        <li class="activeSlide">
          <a href="<?php echo $image; ?>"><img alt="mdx-<?php echo $thumbkey; ?>" class="attachment-thumbnail" src="<?php echo $image; ?>"></a>
        </li>
      <?php } ?>
        
      </ul>
    </div>
  </div><span class="hide-for-small"></span>
  <div class="specs side-lift-block">
    <span class="hide-for-small"></span>
    <ul class="refine-nav">
      <li class="first active">
        <span class="hide-for-small"><span>Full Specifications</span></span>
        <ul>
          <li>
            <p class="strong">Technical:</p>
            <br />
            <?php
            	echo nl2br($feature['Technical']);
            ?>
          </li>
          <li>
            <p class="strong"><br />Safety:</p>
            <br />
            <?php
            	echo nl2br($feature['Safety']);
            ?>
          </li>
          <li>
            <p class="strong"><br />Exterior:</p>
            <br />
            <?php
            	echo nl2br($feature['Exterior']);
            ?>
          </li>
        </ul>
      </li>
      <!--<li class="second">
        <span>Warranty</span>
        <ul style="display: none;">
          <li>5-Day Money-Back Guarantee At Car Dealer, we know that not every
          car is perfect for every person, so all used Car Dealer cars come
          with our 5-Day Money-Back Guarantee. You can return any car for any
          reason within a 5-day period. Simply bring it back in the condition
          in which it was purchased, and you'll get a full refund.</li>
        </ul>
      </li>-->
       
    </ul>
  </div>
  <div class="tabs hideOnSearch">
    <span class="contact-tab active">Contact Us</span> <span class=
    "features-tab">Features</span> <span class="video-tab">Video</span>
    <div class="item-list">
      <ul class="features features-list" style="display: none;">
      	<?php
      		$conv = explode("\n",$feature['Convenience']);
      		
      		foreach($conv as $con){
	      		echo '<li>'.$con.'</li>';
      		}
      	?>
      </ul>
      <div class="features" style="display: none; margin-left: 2em">
       </div>
      <ul class="video" style="display: none;">
        <li><iframe frameborder="0" height="310" src=
        "http://www.youtube.com/embed/ZJJM1s9mQP0" width="100%"></iframe></li>
      </ul>
      <ul class="contact first active" style="display: block;">
        <li>
          <div class="right-block">
            <div class="contact-seller-block">
              <form action="" class="seller-contact-form" id="contactFormPage"
              method="post" name="contactFormPage">
                <p><input class="seller-input-bar" id="contactName" name=
                "contactName"  
                type="text" value="Name"></p><input class="seller-input-bar"
                id="phoneNumber" name="phoneNumber" type="text" value="Phone">
                <p></p>
                <p><input class="seller-input-bar" id="email" name="email"
                type="text" value="E-mail"></p>
                <p>
                <textarea class="required requiredField message-box2" cols="8"
                name="comments" rows="5">
I would like to request more information about your vehicle:
<?php echo $detail[0]->name; ?>
</textarea></p>
                <p></p>
                <div class="captcha_form">
                  <span class="error"></span> <label for=
                  "commentsText">Security Text</label> <img src=
                  "http://gorillathemes.com/demo/automax/wp-content/themes/automax_deluxe/includes/captcha/CaptchaSecurityImages4.php?width=100&amp;height=40&amp;characters=5">
                   <label for="security_code4">Security Code:</label>
                  <input id="security_code4" name="security_code4" type="text"
                  value="">
                  <div class="clear"></div>
                </div>
                <div class="clear"></div><input class="seller-send-btn" id=
                "submitsearch" name="submit" type="submit" value="Send">
                <p></p>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <div style="clear:both;"></div>
</div>		
		
<?php }
	
	}else{
		echo '<h4 style="text-align:center;color:red">Car details not found. Please make sure Custom Search Engine plugin is active.</h4>';
	}

?>	

<!-----for tabs and collapse manu---->
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/script.js'></script>
<script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.cycle.min.js'></script>
 <script type='text/javascript' src='<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.colorbox-min.js'></script>
		

<?php }
genesis();