<?php 
if (!session_id())
    session_start();
/*
Plugin Name: Custom Search Engine
Plugin URI: /
Description: Custom search engine for vehicle information from other website sources.
Version: 1.0
Author: Mark Aseo
Author URI: /
*/

global $csewp_db_version;
$csewp_db_version = '1.0';

global $table_name;
$table_name = 'wp_cse_list';

function csewp_install(){
	global $wpdb;
	global $csewp_db_version;
	global $table_name;
	
	
	$charset_collate = $wpdb->get_charset_collate();
	
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `name` varchar(255) DEFAULT NULL,
			  `make` varchar(255) DEFAULT NULL,
			  `model` varchar(255) DEFAULT NULL,
			  `year` year(4) DEFAULT NULL,
			  `vin` varchar(255) DEFAULT NULL,
			  `mileage` int(11) DEFAULT NULL,
			  `engine` varchar(255) DEFAULT NULL,
			  `transmission` varchar(255) DEFAULT NULL,
			  `price` double(20,2) DEFAULT NULL,
			  `color` varchar(20) DEFAULT NULL,
			  `body_style` varchar(255) DEFAULT NULL,
			  `images` longtext,
			  `features` longtext,
			  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  `source` varchar(50) DEFAULT NULL,
			  PRIMARY KEY (`id`),
			  KEY `make` (`make`),
			  KEY `model` (`model`),
			  KEY `year` (`year`),
			  KEY `mileage` (`mileage`),
			  KEY `engine` (`engine`),
			  KEY `transmission` (`transmission`),
			  KEY `body_style` (`body_style`)
	) $charset_collate;";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	
	dbDelta( $sql );

	add_option( 'csewp_db_version', $csewp_db_version );
	
}

register_activation_hook( __FILE__, 'csewp_install' );

function wpcse_findcar($limit=12,$where=null,$order=' ORDER BY RAND() '){
	global $wpdb, $table_name;
	
	if(!empty($limit) && is_integer($limit)){
		$limit_offset = " LIMIT 0, ".$limit;
	}else{
		$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
		
		$query = wpcse_query();
	
		$limit = $query->limit;
		$offset = ($paged - 1) * $limit;
		$limit_offset = " LIMIT {$offset}, {$limit}";
	}
	
	$results = $wpdb->get_results( 'SELECT * FROM ' . $table_name . ' WHERE vin IS NOT NULL '. $where . ' GROUP BY vin ' .$order . $limit_offset , OBJECT );
	return ($wpdb->num_rows > 0) ? $results : false;
}

function wpcse_findcar_count($where=null){
	global $wpdb, $table_name;
	$query = wpcse_query();
	$where = $query->where;
	$result = $wpdb->get_var( 'SELECT COUNT(DISTINCT(vin)) FROM ' . $table_name . ' WHERE vin IS NOT NULL '.$where );
	return $result;
}

function wpcse_filter($col=null,$sort=' ASC',$where=null){
	global $wpdb, $table_name;
	$results = $wpdb->get_results( 'SELECT DISTINCT('. $col .') FROM '. $table_name .' WHERE '. $col .' IS NOT NULL '. $where .' ORDER BY '. $col . $sort, OBJECT );
	return ($wpdb->num_rows > 0) ? $results : false;
}

function wpcse_filter_ajax(){
	$make = urldecode($_POST['make']);
	$model = urldecode($_POST['model']);
	
	$and_model = (!empty($model)) ? ' AND model = "'.$model.'"' : '';
	
	$result_model = wpcse_filter('model','',' AND make = "'.$make.'"');
	$result_year = wpcse_filter('year',' DESC',' AND make = "'.$make.'"'.$and_model);
	
	$option = array();
	
	if($result_model){
		$option['model'] = "";
		foreach($result_model as $k=>$model){
			$option['model'] .= "<option value='".urlencode($model->model)."'>". $model->model ."</option>";
		}
	}
	
	if($result_year){
		$option['year'] = "";
		foreach($result_year as $k=>$year){
			$option['year'] .= "<option value='".urlencode($year->year)."'>". $year->year ."</option>";
		}
	}
	
	echo json_encode($option);
	die();
}

function wpcse_reset(){
	if(isset($_SESSION['search']))
		unset($_SESSION['search']);
}

function wpcse_query(){

	$make = $model = $year = $price = $where = '';
	$limit = 25;
	$sort = ' ORDER BY price DESC';
	
		if(isset($_GET['unset'])){
			$unset = $_GET['unset'];
			
			if(isset($_SESSION['search'][$unset])) unset($_SESSION['search'][$unset]);
			echo '<script>window.location = "'.site_url().'/results/";</script>';			
			exit;
		}
		
		if((isset($_POST) && !empty($_POST)) || isset($_GET['selectmake']) || isset($_GET['selectmodel'])){
			
			if(isset($_REQUEST['selectmake'])) $make = urldecode($_REQUEST['selectmake']);
			if(isset($_REQUEST['selectmodel'])) $model = urldecode($_REQUEST['selectmodel']); 
			if(isset($_REQUEST['selectyear'])) $year = $_REQUEST['selectyear']; 
			if(isset($_REQUEST['selectprice'])) $price = $_REQUEST['selectprice'];
			
			$_SESSION['search']['make'] = $make;
			$_SESSION['search']['model'] = $model;
			$_SESSION['search']['year'] = $year;
			$_SESSION['search']['price'] = $price;
			
			echo '<script>window.location = "'.site_url().'/results/";</script>';			
			exit;
		}
		
		if(isset($_GET['sort'])){
			$sort = " ORDER BY " . urldecode($_GET['sort']);
			$_SESSION['search']['sort'] = $sort;
			echo '<script>window.location = "'.site_url().'/results/";</script>';
			exit;
		}
		
		if(isset($_SESSION['search'])){
			$search = $_SESSION['search'];
			
			$make = $search['make'];
			$model = $search['model'];;
			$year = $search['year'];;
			$price = $search['price'];;
			$sort = (isset($search['sort'])) ? $search['sort'] : $sort;
			$limit = (isset($search['limit'])) ? $search['limit'] : $limit;
		}
		
		if(!empty($make)) $where .= " AND make = '".esc_sql($make)."'";
		if(!empty($model)) $where .= " AND model = '".esc_sql($model)."'";
		if(!empty($year)) $where .= " AND year = '".esc_sql($year)."'";
		
		if(!empty($price)){
			$pr = explode("-",$price);
			$where .= " AND price BETWEEN ". (float) $pr[0] ." AND  ". (float) $pr[1];
		}
		
		if(isset($_GET['limit']) && is_numeric($_GET['limit'])){
			$limit = $_GET['limit'];
			$_SESSION['search']['limit'] = $limit;
			echo '<script>window.location = "'.site_url().'/results/";</script>';
			exit;
		}
		
	return (object) array(
		'where'	=> $where,
		'sort'	=> $sort,
		'make'	=> $make,
		'model'	=> $model,
		'year'	=> $year,
		'price'	=> $price,
		'limit' => $limit
	); 	
}

function wpcse_price(){
	$where = wpcse_query();
	$price = array(
		'0-5000' 		=> '$1 - $5000',
		'5001-10000'	=> '$5,001 - $10,000',
		'10001-15000'	=> '$10,001 - $15,000',
		'15001-20000'	=> '$15,001 - $20,000',
		'20001-25000'	=> '$20,001 - $25,000',
		'25001-50000'	=> '$25,001 - $50,000'
	);
	
	foreach($price as $k => $pr){
		$sel = ($k == $where->price) ? ' selected="selected"' : null; 
		echo '<option value="'.$k.'"'.$sel.'>'.$pr.'</option>';
	}
}

function wpcse_sort(){
	$where = wpcse_query();
	$order = array(
		'price DESC' 	=> 'Price - Highest',
		'price ASC'		=> 'Price - Lowest',
		'mileage ASC'	=> 'Mileage - Lowest',
		'mileage DESC'	=> 'Mileage - Highest',
		'year DESC'		=> 'Year - Newest',
		'year ASC'		=> 'Year - Oldest',
		'make,model ASC'=> 'Make/Model - A to Z',
		'make,model DESC'=> 'Make/Model - Z to A'
	);

	foreach($order as $k => $ord){
		$sel = (preg_match('/'.$k.'/',$where->sort)) ? ' selected="selected"' : null; 
		echo '<option value="'.$k.'"'.$sel.'>'.$ord.'</option>';
	}
}

function wpcse_perpage(){
	
	$limit = array(
		10, 25, 50, 75, 100
	);
	
	$query = wpcse_query();
	
	foreach($limit as $lm){
		$sel = ($query->limit == $lm) ? ' selected="selected"' : null; 
		echo '<option value="'.$lm.'"'.$sel.'>'.$lm.'</option>';
	}
}

function wpcse_paginate(){

	$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
	
	$query = wpcse_query();
	$args = array(
		'base'               => site_url().'/results/%_%',
		'format'             => '?page=%#%',
		'total'              => ceil(wpcse_findcar_count() / $query->limit),
		'current'            => $paged,
		'show_all'           => False,
		'end_size'           => 1,
		'mid_size'           => 3,
		'prev_next'          => false,
		'prev_text'          => __('� Previous'),
		'next_text'          => __('Next �'),
		'type'               => 'plain',
		'add_args'           => False,
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => ''
	);
	
	return paginate_links( $args );
}

add_action('wp_ajax_wpcse_filter_callback', 'wpcse_filter_ajax');
add_action('wp_ajax_nopriv_wpcse_filter_callback', 'wpcse_filter_ajax');